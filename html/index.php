<?php
	$wallet = [
		'mac' => 'https://github.com/PinkDev/Pink2/releases/download/v2.0.0.5/Pinkcoin-Qt-OSX-2.0.0.5.dmg',
		'windows' => 'https://github.com/PinkDev/Pink2/releases/download/v2.1.0.0/PinkCoin-Qt-Win64-2.1.zip',
	];
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Pinkcoin is an open-source peer-to-peer digital currency." />

		<title>Pinkcoin</title>
		<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">

		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans">
		<link rel="stylesheet" href="style.css">

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

		<meta property="og:title" content="Pinkcoin -- Micro-tip, Macro Impact!">
		<meta property="og:type" content="article">
		<meta property="og:url" content="http://getstarted.with.pink/">
		<meta property="og:image" content="http://getstarted.with.pink/images/pink_flower_small.png">
		<meta property="og:description" content="Driven by our passion and devotion to spread Pinkcoin across the universe.">
		<meta property="fb:app_id" content="202699930219711">

		<meta name="twitter:card" value="summary">
	</head>
	<body>
		<header id="header">
			<div class="container">
				<div class="row">
					<div class="col-sm-7">
						<nav class="text-uppercase">
							<ul class="alt list-inline">
								<li><img id="logo" src="images/pinkcoin_logo.png" alt="" /></li>
								<li><a href="#overview">Overview</a></li>
								<li><a href="https://chainz.cryptoid.info/pink/" target="_blank">Explorer</a></li>
								<li><a href="http://donate.with.pink/">Donate</a></li>
								<li><a href="#exchanges">Exchanges</a></li>
								<li><a href="#wallet"> Wallet</a></li>
								<li><a href="https://bitcointalk.org/index.php?topic=1905864.0" target="_blank"> BitcoinTalk</a></li>
								<li><a href="https://drive.google.com/file/d/0B_NhzbJTemMfQkF5eUlJUnJ0eGM/view" target="_blank"> Pinkpaper</a></li>
							</ul>
						</nav>
					</div>
					<div class="col-sm-5">
						<nav class="text-right text-uppercase">
							<ul class="list-inline">
								<li><a href="http://swap.with.pink" target="_blank">Coin Swap</a></li>
								<li><a href="https://pinkbuffaloz.com/" target="_blank">Blog</a></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h1>
							<img src="images/pinkcoin.png" alt="Pinkcoin" class="img-responsive center-block" />
						</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 text-center text-uppercase">
						<h2 class="slogan">What is Pinkcoin?<br />Read our PinkPaper to learn more about our coin and mission</h2>
					</div>
				</div>
				<div class="row padding">
					<div class="col-sm-12 text-center text-uppercase">
						<a href="https://drive.google.com/file/d/0B_NhzbJTemMfQkF5eUlJUnJ0eGM/view" class="btn">Read our PinkPaper</a>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<div id="wallet-slice"></div>
					</div>
				</div>
			</div>
		</header>

		<div class="container padding">
			<!-- SUMMARIES -->
			<div class="row padding">
				<div class="col-sm-4 text-center">
					<img src="images/altruistic_members.png" alt="" class="image-tile" />
					<h3 class="text-uppercase">Altruistic<br />Members</h3>
					<p>The <span class="text-alt">#pinkarmy</span> prides itself on kindness,<br>charitable actions, and positive thinking.</p>
				</div>
				<div class="col-sm-4 text-center">
					<img src="images/everyones_welcome.png" alt="" class="image-tile" />
					<h3 class="text-uppercase">Everyones<br />Welcome</h3>
					<p><a href="http://slack.with.pink" target="_blank" class="text-alt">Open to the community</a><br>We look forward to your suggestions.</p>
				</div>
				<div class="col-sm-4 text-center">
					<img src="images/made_with_love.png" alt="" class="image-tile" />
					<h3 class="text-uppercase">Made<br />With Love</h3>
					<p>Driven by our passion and devotion<br>to spread <span class="text-alt">Pinkcoin</span> across the universe.</p>
				</div>
			</div>

			<!-- SPECIFICATIONS -->
			<div id="overview" class="row padding">
				<img src="images/pinkcoin_features.png" alt="#Pinkcoin Features" class="img-responsive center-block">
			</div>
			<div class="row padding">
				<div class="col-sm-6">
					<h3 class="text-alt text-uppercase">Specifications</h3>
					<ul class="list-unstyled">
						<li>Proof of Work: 2 minutes, Reward 50 coins</li>
						<li>Proof of Stake: 6 minutes, Reward 100 coins</li>
						<li>Flash Proof of Stake: 1 minute, Reward 150 coins</li>
					</ul>
					<ul class="list-unstyled">
						<li>Average block time POW/POS: 90 seconds</li>
						<li>Average block time POW/Flash POS: 40 seconds</li>
					</ul>
					<ul class="list-unstyled">
						<li>Rewards halve every 2 years</li>
						<li>Absolute coin cap is 500,000,000 coins.</li>
					</ul>
				</div>
				<div class="col-sm-6">
					<h3 class="text-alt text-uppercase">Flash-Staking</h3>
					<p>It is a classic POW/POS based coin with a fixed POS reward with the added flavour of a new technology: Flash Staking.
					This increases the stake rate and reward structure during pre-selected peak hours of the day.</p>
					<p>This reduces the overall block count over time while retaining the confirmation speed that Pinkcoin is known for.
					There are 4 selected 1 hour durations each day.</p>
				</div>
			</div>
			
			<!-- VIDEOS -->
			<div class="row padding">
				<div class="col-sm-4 text-center">
					<iframe src="https://www.youtube.com/embed/D-xF6XsAVUI" frameborder="0" allowfullscreen></iframe>
					<h3 class="text-uppercase">Staking, Flash Staking <br />and Side Staking</h3>
				</div>
				<div class="col-sm-4 text-center">
					<iframe src="https://www.youtube.com/embed/Td6Zd4sHiqE" frameborder="0" allowfullscreen></iframe>
					<h3 class="text-uppercase">Donate Through<br />  Staking, Donate4Life</h3>
				</div>
				<div class="col-sm-4 text-center">
					<iframe src="https://www.youtube.com/embed/kaQD-GKpgBA" frameborder="0" allowfullscreen></iframe>
					<h3 class="text-uppercase">What does each <br />fund represent?</h3>
				</div>
			</div>

			<!-- EXCHANGES -->
			<div id="exchanges" class="row">
				<div class="col-sm-12">
					<h2 class="text-alt text-center text-uppercase">Exchanges</h2>
				</div>
			</div>
			<div class="row padding">
				<div class="col-sm-3 text-center">
					<a href="https://www.poloniex.com/exchange#btc_pink" target="_blank" class="thumbnail">
						<img src="images/poloniex_logo_small.png" alt="Poloniex exchange"><br/>
					</a><br>
				</div>
				<div class="col-sm-3 text-center">
					<a href="https://bittrex.com/Market/Index?MarketName=BTC-PINK" target="_blank" class="thumbnail">
						<img src="images/bittrex_logo_small.png" alt="Bittrex exchange"><br>
					</a>
				</div>
				<div class="col-sm-3 text-center">
					<a href="https://www.cryptopia.co.nz/Exchange/?market=PINK_BTC" target="_blank" class="thumbnail">
						<img src="images/cryptopia_logo_small.png" alt="Cryptopia exchange"><br>
					</a>
				</div>
				<div class="col-sm-3 text-center">
					<a href="https://novaexchange.com/market/BTC_PINK/" class="thumbnail">
						<img src="images/nova_logo_small.png" alt="Nova exchange"><br>
					</a>
				</div>
			</div>

			<!-- NEW WALLET -->
			<div id="wallet" class="row">
				<div class="col-sm-12">
					<h2 class="text-alt text-center text-uppercase">New Wallet</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 text-center">
					<img src="images/new_pinkcoin_wallet.png" alt="New Pinkcoin Wallet Overview" class="img-responsive center-block" />
				</div>
			</div>
			<div id="downloadwallet" class="row">
				<div class="col-sm-12">
					<h2 class="text-alt text-center text-uppercase">Pick your Wallet</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6 text-center">
					<a href="<?php echo $wallet['windows']; ?>" target="_blank" class="btn btn-alt text-uppercase"><img src="images/windows.png" alt="Download the Windows Wallet" /></a><br />
					<br/>
					<small class="text-muted">Updated theme in development</small>
				</div>
				<div class="col-sm-6 text-center">
					<a href="<?php echo $wallet['mac']; ?>" target="_blank" class="btn btn-alt text-uppercase"><img src="images/mac.png" alt="Download the Windows Wallet" /></a><br />
					<br/>
					<small class="text-muted">Updated theme in development</small>
				</div>
			</div>

			<!-- FOOTER -->
			<div class="row padding">
				<div class="col-sm-12 text-center">
					<h3 class="text-center text-uppercase">
						Join us
					</h3>
				</div>
			</div>
			
			<!-- SOCIAL -->
			<div class="row padding">
				<div class="col-sm-4 text-center">
					<a href="https://twitter.com/Pinkcoin_/" target="_blank" class="thumbnail">
						<img src="images/twitter.png" alt="Official Pinkcoin on twitter"><br>
					</a>
				</div>
				<div class="col-sm-4 text-center">
					<a href="https://www.facebook.com/pinkcoin/" target="_blank" class="thumbnail">
						<img src="images/facebook.png" alt="Official Pinkcoin on Facebook"><br>
					</a>
				</div>
				<div class="col-sm-4 text-center">
					<a href="http://slack.with.pink/" target="_blank" class="thumbnail">
						<img src="images/slack.png" alt="Slack.with.pink"><br>
					</a>
				</div>
			</div>
			
			<div class="row padding">
				<div class="col-sm-12 text-center">
					<small class="text-muted">Copyright &copy; 2017</small><br>
					<small class="text-muted">Made in Canada</small>
				</div>
			</div>
		</div>
	</body>
</html>